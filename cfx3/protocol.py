import struct
import typing
from dataclasses import dataclass
from enum import Enum, auto

DataTypes = typing.Union[int, float, str, typing.List[int], typing.List[float]]


class DataType(Enum):
    UTF8_STRING = auto()
    INT8_NUMBER = auto()
    UINT8_NUMBER = auto()
    INT8_BOOL = auto()
    INT16_ARRAY = auto()
    INT16_DECI_DEGREE_CELSIUS = auto()
    INT16_DECI_VOLT = auto()
    HISTORY_DATA_ARRAY = auto()
    UNKNOWN = auto()


class Metric(Enum):
    PRODUCT_SERIAL_NUMBER = auto()

    COMPARTMENT_COUNT = auto()
    ICEMAKER_COUNT = auto()
    COMPARTMENT_0_POWER = auto()
    COMPARTMENT_1_POWER = auto()
    ICEMAKER_POWER = auto()
    COMPARTMENT_0_TEMPERATURE = auto()
    COMPARTMENT_1_TEMPERATURE = auto()
    COMPARTMENT_0_SET_TEMPERATURE = auto()
    COMPARTMENT_1_SET_TEMPERATURE = auto()
    COMPARTMENT_0_RECOMMENDED_RANGE = auto()
    COMPARTMENT_1_RECOMMENDED_RANGE = auto()
    COMPARTMENT_0_TEMPERATURE_RANGE = auto()
    COMPARTMENT_1_TEMPERATURE_RANGE = auto()
    COMPARTMENT_0_DOOR_OPEN = auto()
    COMPARTMENT_1_DOOR_OPEN = auto()
    COOLER_POWER = auto()
    PRESENTED_TEMPERATURE_UNIT = auto()
    BATTERY_VOLTAGE_LEVEL = auto()
    BATTERY_PROTECTION_LEVEL = auto()
    POWER_SOURCE = auto()

    COMPARTMENT_0_TEMPERATURE_HISTORY_HOUR = auto()
    COMPARTMENT_1_TEMPERATURE_HISTORY_HOUR = auto()
    COMPARTMENT_0_TEMPERATURE_HISTORY_DAY = auto()
    COMPARTMENT_1_TEMPERATURE_HISTORY_DAY = auto()
    COMPARTMENT_0_TEMPERATURE_HISTORY_WEEK = auto()
    COMPARTMENT_1_TEMPERATURE_HISTORY_WEEK = auto()
    DC_CURRENT_HISTORY_HOUR = auto()
    DC_CURRENT_HISTORY_DAY = auto()
    DC_CURRENT_HISTORY_WEEK = auto()

    COMMUNICATION_ALARM = auto()
    NTC_OPEN_LARGE_ERROR = auto()
    NTC_OPEN_SMALL_ERROR = auto()
    NTC_SHORT_LARGE_ERROR = auto()
    NTC_SHORT_SMALL_ERROR = auto()
    FAN_OVERVOLTAGE_ERROR = auto()
    SOLENOID_VALVE_ERROR = auto()
    COMPRESSOR_START_FAIL_ERROR = auto()
    COMPRESSOR_SPEED_ERROR = auto()
    CONTROLLER_OVER_TEMPERATURE = auto()
    TEMPERATURE_ALERT_DCM = auto()
    TEMPERATURE_ALERT_CC = auto()
    DOOR_ALERT = auto()
    VOLTAGE_ALERT = auto()

    DEVICE_NAME = auto()
    WIFI_MODE = auto()
    BLUETOOTH_MODE = auto()
    WIFI_AP_CONNECTED = auto()
    STATION_SSID_0 = auto()
    STATION_SSID_1 = auto()
    STATION_SSID_2 = auto()
    STATION_PASSWORD_0 = auto()
    STATION_PASSWORD_1 = auto()
    STATION_PASSWORD_2 = auto()
    STATION_PASSWORD_3 = auto()
    STATION_PASSWORD_4 = auto()
    CFX_DIRECT_PASSWORD_0 = auto()
    CFX_DIRECT_PASSWORD_1 = auto()
    CFX_DIRECT_PASSWORD_2 = auto()
    CFX_DIRECT_PASSWORD_3 = auto()
    CFX_DIRECT_PASSWORD_4 = auto()


class Unit(Enum):
    CELCIUS = auto()
    VOLTS = auto()
    BOOL = auto()
    NONE = auto()


@dataclass
class Field:
    data: DataTypes
    last_updated: float
    unit: Unit

    def to_friendly(self):
        return FriendlyField(data=self.data, last_updated=self.last_updated, unit=self.unit.name)


class FriendlyField:
    data: DataTypes
    last_updated: float
    unit: str


COMMAND_ID_TO_METRIC = {
    # [0, 128, 0, 1]
    0x1008000: (Metric.COMPARTMENT_COUNT, DataType.INT8_NUMBER, Unit.NONE),
    # [0, 129, 0, 1]
    0x1008100: (Metric.ICEMAKER_COUNT, DataType.INT8_NUMBER, Unit.NONE),
    # [0, 0, 1, 1]
    0x1010000: (Metric.COMPARTMENT_0_POWER, DataType.INT8_BOOL, Unit.NONE),
    # [16, 0, 1, 1]
    0x1010010: (Metric.COMPARTMENT_1_POWER, DataType.INT8_BOOL, Unit.NONE),
    # [0, 6, 3, 1]
    0x1030600: (Metric.ICEMAKER_POWER, DataType.INT8_BOOL, Unit.NONE),
    # [0, 193, 0, 0]
    0xc100: (Metric.PRODUCT_SERIAL_NUMBER, DataType.UTF8_STRING, Unit.NONE),
    # [0, 1, 1, 1]
    0x1010100: (Metric.COMPARTMENT_0_TEMPERATURE, DataType.INT16_DECI_DEGREE_CELSIUS, Unit.CELCIUS),
    # [16, 1, 1, 1]
    0x1010110: (Metric.COMPARTMENT_1_TEMPERATURE, DataType.INT16_DECI_DEGREE_CELSIUS, Unit.CELCIUS),
    # [0, 2, 1, 1]
    0x1010200: (Metric.COMPARTMENT_0_SET_TEMPERATURE, DataType.INT16_DECI_DEGREE_CELSIUS, Unit.CELCIUS),
    # [16, 2, 1, 1]
    0x1010210: (Metric.COMPARTMENT_1_SET_TEMPERATURE, DataType.INT16_DECI_DEGREE_CELSIUS, Unit.CELCIUS),
    # [0, 129, 1, 1]
    0x1018100: (Metric.COMPARTMENT_0_RECOMMENDED_RANGE, DataType.INT16_ARRAY, Unit.CELCIUS),
    # [16, 129, 1, 1]
    0x1018110: (Metric.COMPARTMENT_1_RECOMMENDED_RANGE, DataType.INT16_ARRAY, Unit.CELCIUS),
    # [0, 128, 1, 1]
    0x1018000: (Metric.COMPARTMENT_0_TEMPERATURE_RANGE, DataType.INT16_ARRAY, Unit.CELCIUS),
    # [16, 129, 1, 1]
    0x1018010: (Metric.COMPARTMENT_1_TEMPERATURE_RANGE, DataType.INT16_ARRAY, Unit.CELCIUS),
    # [0, 8, 1, 1]
    0x1010800: (Metric.COMPARTMENT_0_DOOR_OPEN, DataType.INT8_BOOL, Unit.BOOL),
    # [16, 8, 1, 1]
    0x1010810: (Metric.COMPARTMENT_1_DOOR_OPEN, DataType.INT8_BOOL, Unit.BOOL),
    # [0, 0, 3, 1]
    0x1030000: (Metric.COOLER_POWER, DataType.INT8_BOOL, Unit.BOOL),
    # [0, 0, 2, 1]
    0x1020000: (Metric.PRESENTED_TEMPERATURE_UNIT, DataType.INT8_NUMBER, Unit.NONE),
    # [0, 1, 3, 1]
    0x1030100: (Metric.BATTERY_VOLTAGE_LEVEL, DataType.INT16_DECI_VOLT, Unit.VOLTS),
    # [0, 2, 3, 1]
    0x1030200: (Metric.BATTERY_PROTECTION_LEVEL, DataType.UINT8_NUMBER, Unit.NONE),
    # [0, 5, 3, 1]
    0x1030500: (Metric.POWER_SOURCE, DataType.INT8_NUMBER, Unit.NONE),

    # [0, 64, 1, 1]
    0x1014000: (Metric.COMPARTMENT_0_TEMPERATURE_HISTORY_HOUR, DataType.HISTORY_DATA_ARRAY, Unit.CELCIUS),
    # [16, 64, 1, 1]
    0x1014010: (Metric.COMPARTMENT_1_TEMPERATURE_HISTORY_HOUR, DataType.HISTORY_DATA_ARRAY, Unit.CELCIUS),
    # [0, 65, 1, 1]
    0x1014100: (Metric.COMPARTMENT_0_TEMPERATURE_HISTORY_DAY, DataType.HISTORY_DATA_ARRAY, Unit.CELCIUS),
    # [16, 65, 1, 1]
    0x1014110: (Metric.COMPARTMENT_1_TEMPERATURE_HISTORY_DAY, DataType.HISTORY_DATA_ARRAY, Unit.CELCIUS),
    # [0, 66, 1, 1]
    0x1014200: (Metric.COMPARTMENT_0_TEMPERATURE_HISTORY_WEEK, DataType.HISTORY_DATA_ARRAY, Unit.CELCIUS),
    # [16, 66, 1, 1]
    0x1014210: (Metric.COMPARTMENT_1_TEMPERATURE_HISTORY_WEEK, DataType.HISTORY_DATA_ARRAY, Unit.CELCIUS),
    # [0, 64, 3, 1]
    0x1034000: (Metric.DC_CURRENT_HISTORY_HOUR, DataType.HISTORY_DATA_ARRAY, Unit.NONE),
    # [0, 65, 3, 1]
    0x1034100: (Metric.DC_CURRENT_HISTORY_DAY, DataType.HISTORY_DATA_ARRAY, Unit.NONE),
    # [0, 66, 3, 1]
    0x1034200: (Metric.DC_CURRENT_HISTORY_WEEK, DataType.HISTORY_DATA_ARRAY, Unit.NONE),

    # [0, 3, 4, 1]
    0x1040300: (Metric.COMMUNICATION_ALARM, DataType.INT8_BOOL, Unit.BOOL),
    # [0, 1, 4, 1]
    0x1040100: (Metric.NTC_OPEN_LARGE_ERROR, DataType.INT8_BOOL, Unit.BOOL),
    # [0, 17, 4, 1]
    0x1041100: (Metric.NTC_OPEN_SMALL_ERROR, DataType.INT8_BOOL, Unit.BOOL),
    # [0, 2, 4, 1]
    0x1040200: (Metric.NTC_SHORT_LARGE_ERROR, DataType.INT8_BOOL, Unit.BOOL),
    # [0, 18, 4, 1]
    0x1041200: (Metric.NTC_SHORT_SMALL_ERROR, DataType.INT8_BOOL, Unit.BOOL),
    # [0, 50, 4, 1]
    0x1043200: (Metric.FAN_OVERVOLTAGE_ERROR, DataType.INT8_BOOL, Unit.BOOL),
    # [0, 9, 4, 1]
    0x1040900: (Metric.SOLENOID_VALVE_ERROR, DataType.INT8_BOOL, Unit.BOOL),
    # [0, 51, 4, 1]
    0x1043300: (Metric.COMPRESSOR_START_FAIL_ERROR, DataType.INT8_BOOL, Unit.BOOL),
    # [0, 52, 4, 1]
    0x1043400: (Metric.COMPRESSOR_SPEED_ERROR, DataType.INT8_BOOL, Unit.BOOL),
    # [0, 53, 4, 1]
    0x1043500: (Metric.CONTROLLER_OVER_TEMPERATURE, DataType.INT8_BOOL, Unit.BOOL),
    # [0, 3, 5, 1]
    0x1050300: (Metric.TEMPERATURE_ALERT_DCM, DataType.INT8_BOOL, Unit.BOOL),
    # [0, 0, 4, 1]
    0x1040000: (Metric.TEMPERATURE_ALERT_CC, DataType.INT8_BOOL, Unit.BOOL),
    # [0, 1, 5, 1]
    0x1050100: (Metric.DOOR_ALERT, DataType.INT8_BOOL, Unit.BOOL),
    # [0, 2, 5, 1]
    0x1050200: (Metric.VOLTAGE_ALERT, DataType.INT8_BOOL, Unit.BOOL),

    # [0, 0, 6, 1]
    0x1060000: (Metric.DEVICE_NAME, DataType.UTF8_STRING, Unit.NONE),
    # [0, 1, 6, 1]
    0x1060100: (Metric.WIFI_MODE, DataType.INT8_BOOL, Unit.NONE),
    # [0, 3, 6, 1]
    0x1060300: (Metric.BLUETOOTH_MODE, DataType.INT8_BOOL, Unit.NONE),
    # [0, 8, 6, 1]
    0x1060800: (Metric.WIFI_AP_CONNECTED, DataType.INT8_BOOL, Unit.NONE),
    # [0, 0, 7, 1]
    0x1070000: (Metric.STATION_SSID_0, DataType.UTF8_STRING, Unit.NONE),
    # [1, 0, 7, 1]
    0x1070001: (Metric.STATION_SSID_1, DataType.UTF8_STRING, Unit.NONE),
    # [2, 0, 7, 1]
    0x1070002: (Metric.STATION_SSID_2, DataType.UTF8_STRING, Unit.NONE),
    # [0, 1, 7, 1]
    0x1070100: (Metric.STATION_PASSWORD_0, DataType.UTF8_STRING, Unit.NONE),
    # [1, 1, 7, 1]
    0x1070101: (Metric.STATION_PASSWORD_1, DataType.UTF8_STRING, Unit.NONE),
    # [2, 1, 7, 1]
    0x1070102: (Metric.STATION_PASSWORD_2, DataType.UTF8_STRING, Unit.NONE),
    # [3, 1, 7, 1]
    0x1070103: (Metric.STATION_PASSWORD_3, DataType.UTF8_STRING, Unit.NONE),
    # [4, 1, 7, 1]
    0x1070104: (Metric.STATION_PASSWORD_4, DataType.UTF8_STRING, Unit.NONE),
    # [0, 2, 7, 1]
    0x1070200: (Metric.CFX_DIRECT_PASSWORD_0, DataType.UTF8_STRING, Unit.NONE),
    # [1, 2, 7, 1]
    0x1070201: (Metric.CFX_DIRECT_PASSWORD_1, DataType.UTF8_STRING, Unit.NONE),
    # [2, 2, 7, 1]
    0x1070202: (Metric.CFX_DIRECT_PASSWORD_2, DataType.UTF8_STRING, Unit.NONE),
    # [3, 2, 7, 1]
    0x1070203: (Metric.CFX_DIRECT_PASSWORD_3, DataType.UTF8_STRING, Unit.NONE),
    # [4, 2, 7, 1]
    0x1070204: (Metric.CFX_DIRECT_PASSWORD_4, DataType.UTF8_STRING, Unit.NONE),
}


class ParsingError(Exception):
    pass


class DecodingError(Exception):
    pass


class MalformedData(Exception):
    pass


class UnknownCommandId(Exception):
    pass


def parse_command_id(data: typing.List[int]) -> int:
    return struct.unpack('<i', bytearray(data))[0]


def parse_data(raw_data: list) -> typing.Tuple[Metric, DataTypes, Unit]:
    # TODO: Maybe don't use lists for all this.
    # The first digit is always a 0, for some reason
    if raw_data[0] != 0:
        raise ParsingError("First byte was not a 0")
    # The command ID is 4 bytes
    command_id = parse_command_id(raw_data[1:5])
    try:
        metric, datatype, unit = COMMAND_ID_TO_METRIC[command_id]
    except KeyError:
        raise UnknownCommandId(
            f"Unknown command id {hex(command_id)} / {raw_data[1:5]}")

    deci = unit in (Unit.CELCIUS, Unit.VOLTS)

    data = raw_data[5:]
    decoded_data = decode_data(data, datatype, deci)

    return metric, decoded_data, unit


def decode_data(data: typing.List[int], datatype: DataType, deci: bool) -> DataTypes:
    bytes = bytearray(data)
    match datatype:
        case DataType.UTF8_STRING:
            # TODO: fix "1111111\u0000\u0000\u0000\u0000\u0000\u0000\u0000"
            return bytes.decode(encoding="UTF8")
        case DataType.INT8_NUMBER:
            if len(data) != 1:
                raise DecodingError(f"Expected 1 byte, got {len(data)}")
            # TODO: this is fishy
            return data[0]
        case DataType.UINT8_NUMBER:
            if len(data) != 1:
                raise DecodingError(f"Expected 1 byte, got {len(data)}")
            # TODO: this is fishy
            return data[0]
        case DataType.INT8_BOOL:
            if len(data) != 1:
                raise DecodingError(f"Expected 1 byte, got {len(data)}")
            return bool(data[0])
        case DataType.INT16_ARRAY:
            if len(bytes) < 2 or len(bytes) % 2 != 0:
                raise DecodingError(
                    f"Expected >2 or multiple of 2 bytes, got {len(bytes)}")
            array_length = int(len(bytes) / 2)
            format = f'<{array_length}h'
            data_array = list(struct.unpack(format, bytes))
            if deci:
                data_array = [d / 10 for d in data_array]
            return data_array
        case DataType.HISTORY_DATA_ARRAY:  # TODO: history data array unconfirmed
            # Need to strip a trailing byte off, no idea what it means yet
            bytes = bytes[:-1]
            if len(bytes) < 2 or len(bytes) % 2 != 0:
                raise DecodingError(
                    f"Expected >2 or multiple of 2 bytes, got {len(bytes)}")
            array_length = int(len(bytes) / 2)
            format = f'<{array_length}h'
            data_array = list(struct.unpack(format, bytes))
            if deci:
                data_array = [d / 10 for d in data_array]
            return data_array
        case DataType.INT16_DECI_DEGREE_CELSIUS | DataType.INT16_DECI_VOLT:
            if len(bytes) != 2:
                raise DecodingError(f"Expected 2 bytes, got {len(bytes)}")
            deci_data = (struct.unpack('<h', bytes)[0] / 10)
            return deci_data
        case _:
            raise DecodingError(f"Unknown data type: {datatype}")
