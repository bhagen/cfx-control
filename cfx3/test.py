import struct

temp = 10.1
temp_bytes = bytearray(struct.pack('<h', int(temp * 10)))

print(struct.unpack('<h', temp_bytes)[0] / 10)
