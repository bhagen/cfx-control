import copy
import json
import logging
import queue
import select
import socket
import struct
import sys
import threading
import time
import typing
from dataclasses import dataclass

import protocol

log = logging.getLogger(__name__)

DISCOVER_STRING = 'DDMD'.encode('ASCII')
DISCOVER_PORT = 13142


@dataclass
class DiscoveredFridge():
    address: str
    discover_port: int
    version: int
    pid: int
    id: str
    name: str


class Fridge:
    PING_INTERVAL = 2

    # These aren't really ping/pongs, I don't really know exactly what they do.
    # But, I do know that we have to send a [2], and the fridge should respond with a [4],
    # then after sending a subscribe command, we should send a [4] to get more data. Or
    # something like that...
    PING = [2]
    PONG = [4]
    # Not sure if this actually represents an error, but the fridge seems to send it when it receives data it doesn't know what to do with.
    # It also seems to send it every 5 seconds or so... Who knows.
    ERROR = [6]
    POWER_ON = [0, 0, 0, 3, 1, 1]
    POWER_OFF = [0, 0, 0, 3, 1, 0]
    TEMP_COMMAND_PARTIAL = [0, 0, 2, 1, 1]
    SUBSCRIBE_SINGLE_ZONE = [1, 1, 0, 0, 129]
    SUBSCRIBE_SINGLE_ZONE_ICE = [1, 2, 0, 0, 129]
    SUBSCRIBE_DUAL_ZONE = [1, 3, 0, 0, 129]
    # TODO: format commands using hex and such

    @dataclass
    class Status():
        power: typing.Optional[bool]
        current_temp: typing.Optional[float]
        set_temp: typing.Optional[float]
        battery_voltage: typing.Optional[float]
        last_updated: typing.Optional[float]

    def __init__(self, discovered_fridge: DiscoveredFridge):
        self.name = discovered_fridge.name
        self.address = discovered_fridge.address
        # May not necessarily need to use this port, but we might as well
        self.port = discovered_fridge.discover_port

        self._ping_interval = Fridge.PING_INTERVAL

        self._stop_event = threading.Event()
        self._command_queue: queue.Queue[list[int]] = queue.Queue()
        self._received_queue: queue.Queue[list[int]] = queue.Queue()

        self._fields: typing.Dict[protocol.Metric, protocol.Field] = {}

        self._socket_thread = threading.Thread(target=self._manage_socket)
        self._socket_thread.start()

        self._main_loop_thread = threading.Thread(target=self._loop)
        self._main_loop_thread.start()

        self._initialized = False
        self._subscribed = False

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        # TODO: handle exceptions
        self.disconnect()

    def disconnect(self):
        logging.error("Disconnecting from fridge")
        self._stop_event.set()

    def get_fields(self):
        return copy.deepcopy(self._fields)

    def get_fields_friendly(self):
        fields = self.get_fields()
        return {metric.name: field.to_friendly() for (metric, field) in fields.items()}

    def get_status(self):
        fields = self.get_fields()

        cooler_power = fields.get(protocol.Metric.COOLER_POWER, None)
        current_temp = fields.get(
            protocol.Metric.COMPARTMENT_0_TEMPERATURE, None)
        set_temp = fields.get(
            protocol.Metric.COMPARTMENT_0_SET_TEMPERATURE, None)
        battery_voltage = fields.get(
            protocol.Metric.BATTERY_VOLTAGE_LEVEL, None)

        fields = [cooler_power, current_temp, set_temp, battery_voltage]
        last_updated_fields = [
            field.last_updated for field in fields if field is not None]

        last_updated = None
        if last_updated_fields:
            last_updated = max(last_updated_fields)

        return Fridge.Status(
            power=cooler_power.data if cooler_power else None,
            current_temp=current_temp.data if current_temp else None,
            set_temp=set_temp.data if set_temp else None,
            battery_voltage=battery_voltage.data if battery_voltage else None,
            last_updated=last_updated,
        )

    def set_temp(self, temp: int):
        # TODO: this seems like it could be done better
        deci_degrees_temp = int(temp * 10)
        temp_bytes = bytearray(struct.pack('<h', deci_degrees_temp))
        array = Fridge.TEMP_COMMAND_PARTIAL + [temp_bytes[0], temp_bytes[1]]
        self._command_queue.put(array)

    def set_power(self, power: bool):
        command = Fridge.POWER_ON if power else Fridge.POWER_OFF
        self._command_queue.put(command)

    def send_raw(self, arr: typing.List[int]):
        self._command_queue.put(arr)

    @staticmethod
    def _format_command(data: list) -> bytes:
        return bytes(json.dumps({"ddmp": data}) + '\r', encoding='utf-8')

    def _handle_data(self, raw_data: list):
        try:
            metric, parsed_data, unit = protocol.parse_data(raw_data)
        # TODO: less broad
        except protocol.UnknownCommandId as e:
            logging.error(e)
            return
        except Exception:
            logging.error(f"Failed to handle {raw_data}", exc_info=True)
            return

        if metric not in self._fields:
            logging.debug(f"Adding new field {metric}")
            self._fields[metric] = protocol.Field(0, 0, unit)
        else:
            logging.debug(f"Updating existing field {metric}")

        field = self._fields[metric]
        field.data = parsed_data
        field.last_updated = time.time()

    def _manage_socket(self):
        logging.debug("Starting socket loop")
        while not self._stop_event.is_set():
            with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
                logging.info("Connecting to fridge at %s:%s",
                             self.address, self.port)
                sock.connect((self.address, self.port))
                logging.info("Connected")

                while not self._stop_event.is_set():
                    ready, _, _ = select.select([sock], [], [], 1)
                    for ready_sock in ready:
                        raw = ready_sock.recv(1024)
                        try:
                            data = json.loads(raw)
                        # TODO: less broad
                        except Exception:
                            # TODO: handle ERROR:root:Failed to parse b'{"ddmp":[0,0,2,4,1,0]}\r{"ddmp":[6]}\r'
                            logging.error(f"Failed to parse {raw}")
                            continue
                        logging.debug(f"Received {data}")
                        if "ddmp" not in data:
                            raise protocol.MalformedData(
                                f"Data not formatted properly: {data}")
                        self._received_queue.put(data["ddmp"])

                    command = None
                    try:
                        command = self._command_queue.get_nowait()
                    except queue.Empty:
                        pass

                    if command:
                        logging.debug("Sending command %s", command)
                        sock.sendall(self._format_command(command))

    def _loop(self):
        logging.debug("Starting command loop")
        initialized = False
        subscribed = False
        # This way of handling the init state machine is bad and I should figure out a better way
        while not self._stop_event.is_set() and (not initialized or not subscribed):
            command = Fridge.PING if not initialized else Fridge.SUBSCRIBE_SINGLE_ZONE
            self._command_queue.put(command)
            try:
                maybe_pong = self._received_queue.get(timeout=2)
            except queue.Empty:
                logging.debug("Did not get pong return in time")
                time.sleep(2)
                continue
            if maybe_pong == Fridge.PONG:
                logging.debug("Pong received")
                if not initialized:
                    logging.debug("Initialized")
                    initialized = True
                else:
                    logging.debug("Subscribed")
                    subscribed = True
            else:
                logging.error(
                    f"Received unexpected {maybe_pong}")
                time.sleep(2)
                continue
        logging.debug("Comms initialization complete")
        time.sleep(1)
        next_ping = time.monotonic() + self._ping_interval
        while not self._stop_event.is_set():
            try:
                data = self._received_queue.get(timeout=0.1)
            except queue.Empty:
                data = None

            if data:
                if data == Fridge.PING:
                    self._command_queue.put(Fridge.PONG)
                    next_ping = time.monotonic() + self._ping_interval
                # TODO: not sure if we should respond with anything after the fridge sends a [6]
                # if data == Fridge.ERROR:
                #     self._command_queue.put(Fridge.PONG)
                #     next_ping = time.monotonic() + self._ping_interval
                # TODO: make this just check if we received a valid data packet rather than a flow control thingy
                if len(data) > 1:
                    self._command_queue.put(Fridge.PONG)
                    next_ping = time.monotonic() + self._ping_interval
                    self._handle_data(data)

            if time.monotonic() >= next_ping:
                self._command_queue.put(Fridge.PING)
                next_ping = time.monotonic() + self._ping_interval


def discover_fridges(discover_for_s=3):
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        sock.setblocking(False)
        sock.bind(('0.0.0.0', DISCOVER_PORT))

        fridges: typing.Dict[str, DiscoveredFridge] = {}

        end_discovering_at = time.monotonic() + discover_for_s

        while time.monotonic() < end_discovering_at:
            log.debug('Sending discovery packet')
            sock.sendto(DISCOVER_STRING, ('<broadcast>', DISCOVER_PORT))
            ready, _, _ = select.select([sock], [], [], 0.5)
            for ready_sock in ready:
                # TODO: We should probably send a discovery broadcast, then sit back and wait for a few
                # responses rather than having a tight send -> listen for one packet loop.
                # But, I think this should work anyways.
                data, addr = ready_sock.recvfrom(1024)
                if data == DISCOVER_STRING:
                    log.debug(
                        'Received discover string from %s, ignoring', addr)
                    continue
                parsed_data = json.loads(data)
                if parsed_data['id'] in fridges:
                    log.debug('Already heard from %s/%s, ignoring',
                              parsed_data['name'], parsed_data['id'])
                    continue
                parsed_data['address'] = addr[0]
                parsed_data['discover_port'] = addr[1]
                fridge = DiscoveredFridge(**parsed_data)
                fridges[fridge.id] = fridge
                log.debug('Discovered %s/%s at %s',
                          fridge.name, fridge.id, addr)
            time.sleep(1)
    return fridges


def main(stop_event):
    fridges = discover_fridges()
    if not fridges:
        logging.error('Found no fridges')
        sys.exit(1)
    logging.info('Found these fridges: %s', fridges)

    discovered_fridge = list(fridges.values())[0]

    with Fridge(discovered_fridge=discovered_fridge) as fridge:
        while not stop_event.is_set():
            value = input("Command: ")
            logging.debug("Input: %s", value)
            if value == "on":
                logging.info("Setting power to true")
                fridge.set_power(True)
            if value == "off":
                logging.info("Setting power to false")
                fridge.set_power(False)
            if value == "stop":
                break
            if value.isdigit() or value.startswith("-"):
                logging.info(f"Setting temperature to {int(value)}")
                fridge.set_temp(int(value))
            if value.startswith("[") and value.endswith("]"):
                command = value.strip("[]").split(",")
                command = [int(i) for i in command]
                logging.info(f"Sending custom command {command}")
                fridge.send_raw(command)


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    stop_event = threading.Event()
    try:
        main(stop_event)
    except Exception as e:
        stop_event.set()
        raise e
