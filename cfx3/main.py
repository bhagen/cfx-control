import sys
import typing
from fastapi.responses import JSONResponse
from pydantic import BaseModel

from cfx3 import discover_fridges, Fridge

from fastapi import FastAPI

app = FastAPI()

fridges = discover_fridges()
if not fridges:
    print("No fridges found")
    sys.exit(1)
discovered_fridge = fridges[list(fridges.keys())[0]]
print(f"Available fridges: {fridges}, picking {discovered_fridge}")
fridge = Fridge(discovered_fridge=discovered_fridge)


class Power(BaseModel):
    power: bool


@app.post("/power")
async def set_power(power: Power):
    fridge.set_power(power.power)
    return JSONResponse(content={}, status_code=200)


class Temperature(BaseModel):
    temperature_c: int


@app.post("/temperature")
async def set_temperature(temperature: Temperature):
    fridge.set_temp(temp=temperature.temperature_c)
    return JSONResponse(content={}, status_code=200)


@app.get("/fields")
async def get_fields() -> typing.Dict[str, dict]:
    return fridge.get_fields_friendly()


@app.get("/status")
async def get_status() -> Fridge.Status:
    return fridge.get_status()


@app.on_event("shutdown")
def shutdown_event():
    fridge.disconnect()
