# CFX3 Reverse Engineering
The CFX3 can be connected to a WiFi network, which is a nice addition over the CFX's ad-hoc control network.

## Discovery
Discovering a fridge is pretty simple!
The app sends out a UDP broadcast to port 13142 containing the string `DDMD`.
The fridge responds with a UDP info packet sent to port 13142: `{"version":0,"pid":1,"id":"84cca83389dc","name":"Pike-Fridge"}`.
It is not strictly necessary to use port 13142, the fridge will respond to a broadcast packet from ~any port.

## Format
The fridge expects and sends messages in a json-like format with a carriage return at the end.
For example: `b'{"ddmp":[6]}\r'`

## Protocol
Perhaps unsurprisingly, the protocol of CFX3 fridges seems to differ greatly from CFX.

Here is a sequence of data from TCP packets while commanding subsequent temperature changes:
| Hex                                                          | ASCII                          |
| ------------------------------------------------------------ | ------------------------------ |
| `7b2264646d70223a5b302c302c322c312c312c3135362c3235355d7d0d` | `{"ddmp":[0,0,2,1,1,156,255]}` |
| `7b2264646d70223a5b302c302c322c312c312c3136322c3235355d7d0d` | `{"ddmp":[0,0,2,1,1,162,255]}` |
| `7b2264646d70223a5b302c302c322c312c312c3136372c3235355d7d0d` | `{"ddmp":[0,0,2,1,1,167,255]}` |
| `7b2264646d70223a5b302c302c322c312c312c3137332c3235355d7d0d` | `{"ddmp":[0,0,2,1,1,173,255]}` |
| `7b2264646d70223a5b302c302c322c312c312c3137382c3235355d7d0d` | `{"ddmp":[0,0,2,1,1,178,255]}` |
| `7b2264646d70223a5b302c302c322c312c312c3138342c3235355d7d0d` | `{"ddmp":[0,0,2,1,1,184,255]}` |
| `7b2264646d70223a5b302c302c322c312c312c3138392c3235355d7d0d` | `{"ddmp":[0,0,2,1,1,189,255]}` |

All above packets are app -> fridge.
After each packet is sent, the fridge responds with:
| Hex                          | ASCII          |
| ---------------------------- | -------------- |
| `7b2264646d70223a5b345d7d0d` | `{"ddmp":[4]}` |

The fridge and app both send back and forth these single number arrays:
| Hex                          | ASCII          |
| ---------------------------- | -------------- |
| `7b2264646d70223a5b345d7d0d` | `{"ddmp":[4]}` |
| `7b2264646d70223a5b325d7d0d` | `{"ddmp":[2]}` |
| `7b2264646d70223a5b365d7d0d` | `{"ddmp":[6]}` |

The general flow seems to go:
* App sends a ping `{"ddmp":[2]}`
* Fridge sends back a success pong/ack: `{"ddmp":[4]}`
If the fridge doesn't like what it receives, it sends an error/nack: `{"ddmp":[6]}`

Seemingly without prompting, the fridge sends large arrays:
| ASCII                                                                |
| -------------------------------------------------------------------- |
| `{"ddmp":[0,0,64,1,1,45,0,162,0,220,0,220,0,220,0,220,0,220,0,0]}`   |
| `{"ddmp":[0,0,64,3,1,42,0,48,0,7,0,0,0,0,0,0,0,0,0,0]}`              |
| `{"ddmp":[0,0,65,1,1,229,0,244,0,5,1,20,1,13,1,216,0,203,0,170]}`    |
| `{"ddmp":[0,0,65,3,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,170]`               |
| `{"ddmp":[0,0,66,1,1,240,0,215,0,212,0,212,0,203,0,178,0,101,0,71]}` |
| `{"ddmp":[0,0,66,3,1,0,0,0,0,0,0,0,0,0,0,0,0,2,0,71]}`               |

And the app either acks after each large array with `{"ddmp":[4]}`, or requests the large arrays that way.

Possible commands:

Power on: `{"ddmp":[0,0,0,3,1,1]}`

Power off: `{"ddmp":[0,0,0,3,1,0]}`

Temp settings:
| Selected Temp (c) | ASCII                          |
| ----------------- | ------------------------------ |
| 10                | `{"ddmp":[0,0,2,1,1,100,0]}`   |
| 2                 | `{"ddmp":[0,0,2,1,1,20,0]}`    |
| 1                 | `{"ddmp":[0,0,2,1,1,10,0]}`    |
| 0                 | `{"ddmp":[0,0,2,1,1,0,0]}`     |
| -1                | `{"ddmp":[0,0,2,1,1,246,255]}` |
| -5                | `{"ddmp":[0,0,2,1,1,206,255]}` |
| -10               | `{"ddmp":[0,0,2,1,1,156,255]}` |
| -20               | `{"ddmp":[0,0,2,1,1,36,255]}`  |

So, the last two fields of the data array appear to represent a two byte little endian signed short integer, with the temperature multiplied by 10.
The temperature is always commanded in celcius, regardless of the unit selected in the app.

For example, 2 degrees c * 10 = 20, and cheating using Python's `struct` gives: `struct.pack('>h', 20)` = `b'\x14\x00'` = `[20,0]` (kinda, you get the point hopefully)

So for -10c: `struct.pack('<h', -5 * 10)` = `b'\xce\xff'` = `[206,255]`
