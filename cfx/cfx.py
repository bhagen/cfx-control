import logging
import struct
import socket
import threading
from dataclasses import dataclass
from typing import Callable, Optional


@dataclass
class CFXInfoField:
    name: str
    byte_index: int
    byte_span: int
    decoder: Callable = None
    encoder: Callable = None


def calc_mod_256_checksum(data: list):
    data_checksum = sum(data[: -1]) % 256
    return data_checksum


class CFXInfo:
    def __init__(self, raw_data: str):
        self.fmt = 'BBBBBBBBBBBBBBBBBBBB'
        self.exp_header = b'\xcc\x00\x11'
        self._header_field = CFXInfoField('header', 0, 3)
        self._checksum_field = CFXInfoField('checksum', 19, 1)

        self._fields = {
            'req_temp': CFXInfoField('req_temp', 3, 1, decoder=self._get_temp),
            'batt_prot': CFXInfoField('batt_prot'), 5, 1, decoder = None),
            'units': CFXInfoField('units', 6, 1, decoder=None),
            'dc_voltage': CFXInfoField('dc_voltage', 10, 1, decoder=None),
            'curr_temp': CFXInfoField('curr_temp', 11, 1, decoder=self._get_temp),
            'power_source': CFXInfoField('power_source', 14, 1, decoder=None)
        }

        self.data = {}

        self._validate_header(raw_data)
        self._data = self._decode(raw_data)
        self._validate_checksum(self._data)
        self._process(self._data)

    def _validate_header(self, bytes):
        logging.debug(f'validating header of {bytes}')
        recv_header = bytes[self._header_field.byte_index:
                            self._header_field.byte_index + self._header_field.byte_span]
        assert(recv_header == self.exp_header)

    def _validate_checksum(self, data: list):
        logging.debug(f'validating checksum of {bytes}')
        data_checksum = calc_mod_256_checksum(data)
        # data_checksum = sum(data[: -1]) % 256
        assert(data_checksum == data[self._checksum_field.byte_index])

    def _decode(self, bytes):
        logging.debug(f'decoding {bytes}')
        data = struct.unpack(self.fmt, bytes)

        return data

    def _process(self, data):
        logging.debug(f'processing {data}')
        for field, info in self._fields.items():
            logging.debug(f'processing field {field}')
            if info.byte_span != 1:
                logging.error('not implemented')
                # TODO if this is even needed...
                return None

            self.data[info.name] = info.decoder(data[info.byte_index])

        logging.info(f'Processed data {self.data}')

    def _get_temp(self, raw_int: int):
        return raw_int - 50


class CFXCommand:
    def __init__(self, req_temp: Optional[int] = None, req_power: Optional[bool] = None):
        self._req_header = [51, 0, 12]  # 0x33, 0x01, 0x0c
        self._temp_header = [51, 1, 12]  # 0x33, 0x00, 0x0c
        self._pow_header = [51, 5, 12]  # 0x33, 0x05, 0x0c
        self.req_temp = req_temp
        self.req_power = req_power

    def _convert_temp(self, temp: int) -> int:
        return temp + 50

    def encode(self):
        logging.info(f'Encoding command with req temp {self.req_temp}')

        if self.req_temp is None and self.req_power is None:
            header = self._req_header
            req_temp = 0
            req_power = 0

        if self.req_temp is not None:
            header = self._temp_header
            req_temp = self.req_temp
            req_power = True

        if self.req_power is not None:
            header = self._pow_header
            req_power = self.req_power
            req_temp = 0

        fields = {
            'header': header,
            'req_temp': [self._convert_temp(req_temp)],
            'unknown-1': [0, 0, 1],
            'req_power': [int(req_power)],
            'unknown-2': [0, 0, 0, 0, 0, 0]
        }

        int_arr = []
        for value in fields.values():
            int_arr.extend(value)

        checksum = calc_mod_256_checksum(int_arr)
        int_arr.append(checksum)

        byte_str = b''
        for i in int_arr:
            byte_str += bytes([i])
        return byte_str


def simple_server():
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.bind(('127.0.0.1', 55000))

    sock.listen(1)

    while True:
        connection, client_address = sock.accept()
        data = connection.recv(1024)
        cfx_pack = CFXInfo(data)


if __name__ == "__main__":
    logging.getLogger().setLevel(logging.INFO)
    host = '192.168.1.1'
    port = 6378

    c_comm = CFXCommand(req_power=True)
    c_req = CFXCommand()

    sock = socket.socket()
    sock.connect((host, port))

    while True:
        for i in range(50):
            sock.send(c_req.encode())
            data = sock.recv(1024)
            i = CFXInfo(data)

        sock.send(c_comm.encode())
        data = sock.recv(1024)
        i = CFXInfo(data)
