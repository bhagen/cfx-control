# Attempted reverse-engineering of Dometic CFX communication protocol

## High-Level Details:
* Network: 192.168.0.0
* Fridge IP: 192.168.1.1
* Client IP: I've seen 192.168.1.2 and 192.168.1.3
* Fridge Port: 6378
* Protocol: TCP


The app does a call-and-response to get details from the fridge.
App sends a message, fridge responds with its current status.

## Example Response and Breakdown:

`cc 00 11 64 00 01 01 01 0a 00 08 52 00 01 04 00 00 00 00 ad`

Corresponds with a current temp of 32f and a requested temp of 50f

1. [cc 00 11] **Header**
starting byte index: **0**
Seems to be a header. Never changes

2. [64] **Requested Temp**
starting byte index: **3**
Requested temp. Current informal mappings found:

| Real | Hex |
| ---- | --- |
| 50f  | 64  |
| 48f  | 62  |
| -8f  | 2a  |
| -10c | 28  |
| -9c  | 29  |
| -7c  | 2a  |

3. [00] ??
4. [01] **Battery Protection**
starting byte index: **5**
`00` is low, `01` is med, `02` is high

5. [01] C/F units
starting byte index: **6**
`00` is celcius, `01` is farenheit
This does change the output of the requested temp and current temp fields... Annoyingly.

6. [01 0a 00] ??
7. [08] **DC Voltage**
starting byte index: **10**
DC voltage
|  Real  | Hex |
| ------ | --- |
| 11.8v  | 76  |
| 11.3v  | 71  |
| 12.4v  | 7c  |
When on AC it seems to just read `08`

8. [52] **Current Temp**
starting byte index: **11**
Current temp. Current informal mapping found:

| Real | Hex |
| ---- | --- |
| 37f  | 57  |
| 36f  | 56  |
| 34f  | 54  |
| 32f  | 52  |
| -9c  | 29  |
| -7c  | 2a  |

9. [00 01] ??
10. [04] **AC/DC Power** 
starting byte index: **14**
`00` is on DC Power, `04` is on AC
If both are plugged in, since it defaults to AC anyways, it stays at `04`

11. [00 00 00 00] ??
12. [ad] **Checksum**
starting byte index: **19**
Checksum of everything to the left

Calculated as the sum of the bytes % 256

## Example Command and Breakdown:

`33 00 0c 64 00 00 01 01 00 00 00 00 00 00 a5`

Corresponds with a requested temp of 50f

1. [33 00 0c] **Header**
2. [64] **Requested Temp**
3. [00 00 01] ??
4. [01] **Fridge Power**
5. [00 00 00 00 00 00] ??
6. [a5] **Checksum**

Req. Temp and Checksum have same mapping/calculation as above.

### 1
The header for a command determines if the command is just a request
for a response, or an actual command.

`[33 00 0c]` is just a request. I am unsure if any details after this actually matter (like the requested temp)...

`[33 01 0c]` is a temp command. Naturally you don't want to spam this as it won't apply, as far as I can tell. The fridge has a waiting period of a few seconds before the temp actually gets latched in.

`[33 05 0c]` is a power command. 

### 4
Requested fridge power. Needs the corresponding header of `[33 05 0c]`
`00` is off and `01` is on, as usual.